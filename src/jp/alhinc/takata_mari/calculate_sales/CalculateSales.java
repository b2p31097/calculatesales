package jp.alhinc.takata_mari.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] filePath){

		HashMap<String,String> code = new HashMap<String,String>();
		HashMap<String,Long> sales  = new HashMap<String,Long>();
		List<String> saleslist = new ArrayList<String>();
		List<String> salelist = new ArrayList<String>();
		List<Integer> list = new ArrayList<Integer>();

		long salesAmount;
	    long p;
	    BufferedReader br = null;

	    if(filePath.length!=1){
	    	System.out.println("予期せぬエラーが発生しました");
	    	return;
	    }
	    if(!loadCode("^\\d{3}$","支店","branch.lst",code,sales,filePath[0])){
			return;
		}
		if(!loadSales(list,salelist,filePath[0])){
			return;
		}
		//リストに追加したものが連番になっているか確認
        for(int i =1;i<list.size();i++){
			//連番になっていない
        	if(list.get(i)-list.get(i-1)!=1){
        		System.out.println("売上ファイル名が連番になっていません");
        		return;
        	}
        }
		//集計
        for(int i =0; i < salelist.size(); i++){
        	try{//売上ファイルを読み込む
        		File directorys=new File(filePath[0],salelist.get(i));
        		FileReader frs =new FileReader(directorys);
        		br = new BufferedReader(frs);
        		String lines ;
        		while((lines = br.readLine()) != null){
        			//リストに追加,2行分読み込み
        			saleslist.add(lines);
        		}
        		//ファイルの中身が2行以外のとき
        		if(saleslist.size()!=2){
        			System.out.println(salelist.get(i)+"のフォーマットが不正です");
        			return;
        		}
        		//1行目の内容（支店コード）がsalesマップのキーに存在しないとき
        		if(!sales.containsKey(saleslist.get(0))){
        			System.out.println(salelist.get(i)+"の支店コードが不正です");
        			return;
        		}
        		//2行目に数字以外が入っているとき
        		if(!saleslist.get(1).matches("[0-9]+")){
        			System.out.println("予期せぬエラーが発生しました");
        			return;
        		}
				//売上金額をString型からlong型にする
        		p=Long.parseLong(saleslist.get(1));
				//売上金額を加算salesAmountに代入
        		salesAmount = p + sales.get(saleslist.get(0));
				//マップに追加
        		sales.put(saleslist.get(0),salesAmount);
        		if( salesAmount >=10000000000l){
        			System.out.println("合計金額が10桁を超えました");
        			return;
        		}
        	}catch(IOException e){
        		System.out.println("予期せぬエラーが発生しました");
        		return;
        	}finally{
        		if(br!=null){
        			try{
        				br.close();
        			}catch(IOException e){
        				System.out.println("予期せぬエラーが発生しました");
        				return;
        			}
        		}
        	}
			//リストに追加した文字列を削除
        	saleslist.clear();
        }
        if(!print("branch.out",code,sales,filePath[0])){
        	return;
        }
	}


	 public static boolean print(String fileName,HashMap<String,String> code,HashMap<String,Long> sales,String filePath){
		 BufferedWriter bw = null;
		 try{
			 File directory=new File(filePath,fileName);
			 FileWriter fw = new FileWriter(directory);
			 bw = new BufferedWriter(fw);
			 for (Map.Entry<String,String> bar : code.entrySet()) {
				 bw.write(bar.getKey()+","+bar.getValue()+","+sales.get(bar.getKey()));
				 bw.newLine();
			 }
		 }catch(IOException e){
			 System.out.println("予期せぬエラーが発生しました");
			 return false;
		 }finally{
			 if(bw!=null){
				 try{
					 bw.close();
				 }catch(IOException e){
					 System.out.println("予期せぬエラーが発生しました");
					 return false;
				 }
			 }
		 }
		 return true;
	 }

	public static boolean loadCode(String codeContents,String storeName,String fileName,HashMap<String,String> code,HashMap<String,Long> sales,String filePath){
		BufferedReader br = null;
		long sale=0;
		File branchfile = new File(filePath,fileName);

		try{
			if(!branchfile.exists()){
				System.out.println(storeName+"定義ファイルが存在しません");
				return false;
			}
			FileReader fr =new FileReader(branchfile);
			br = new BufferedReader(fr);
			String line ;
			while((line = br.readLine()) != null){
				String str[] = line.split(",");
				if(str.length != 2){
					System.out.println(storeName+"定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!str[0].matches(codeContents)){
					System.out.println(storeName+"定義ファイルのフォーマットが不正です");
					return false;
				}
				code.put(str[0],str[1]);
				sales.put(str[0],sale);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br!=null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean loadSales(List<Integer> list,List<String> salelist,String filePath){
		File cdirectory=new File(filePath);
        File[] files=cdirectory.listFiles();

        for(int i =0;i<files.length;i++){
        	File file=files[i];
        	String fileName = file.getName();

			//8桁かつrcdが含まれているファイルの場合リストに追加
        	if(fileName.matches("\\d{8}.rcd")&&file.isFile()){
        		int lastDotPos=fileName.lastIndexOf('.');
        		String fileLength=fileName.substring(0, lastDotPos);
        		//int型に変換
        		int dif=Integer.parseInt(fileLength);
				//リストにファイル名(拡張子なしint型)を格納
        		list.add(dif);
				//リストにファイル名(拡張子ありString型)を格納
        		salelist.add(fileName);
        	}
        }
        return true;
	}
}

